#!/usr/bin/env python

"""normalize_vcf.py: Remove all sample columns to save space.
                     The sample columns of the SVDB VCF do not contain
                     valuable data because all GT=./1
"""

import argparse
import sys
import logging
_log = logging.getLogger(__name__)

# Expected VCF input on form:
# "1       20616   cluster_643     N       <DEL>   .       PASS    SVTYPE=DEL;END=20724;SVLEN=1108;NSAMPLES=85;OCC=2;FRQ=0.0235;CIPOS=0,0;CIEND=0,0;VARIANTS=|Diag-wgs00-19:10616:10724|Diag-wgs01-20:10616:10724 GT      0/1"

CHROMS = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "MT"]


def input_parser():
    parser = argparse.ArgumentParser(description='Remove unnecessary data from SVDB VCF')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF-file made from SVDB db export')
    parser.add_argument("--sample-id", default="SAMPLE",
                        help="Set name of the sample column")
    return parser.parse_args()


def parse(line):
    CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO = line.split()[:8]
    FORMAT = "GT"
    SAMPLE = "./1"

    variant = [CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, FORMAT, SAMPLE]
    return "\t".join(variant).strip()


def main(file_object, sample_id):
    with file_object:
        for line in file_object:
            if line.startswith("##"):
                print(line.strip())
            elif line.startswith("#CHROM"):
                print("#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	{}".format(sample_id))
            elif line.startswith(tuple(CHROMS)):
                print(parse(line))
            else:
                _log.warning("Dropping VCF line: {}".format(line))


if __name__ == "__main__":
    args = input_parser()
    logging.basicConfig(level=logging.DEBUG)
    if args.file:
        main(args.file, args.sample_id)
