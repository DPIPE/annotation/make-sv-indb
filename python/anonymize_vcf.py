#!/usr/bin/env python

"""anonymize_vcf.py: Remove INFO field VARIANTS from SVDB VCF file
                     In order to do real anonymization, normalize_vcf.py 
                     must be run  on the file first
"""

import argparse
import sys
import logging
from svpack import sv_utils as utils
_log = logging.getLogger(__name__)

# Expected VCF input on form:
# "1       20616   cluster_643     N       <DEL>   .       PASS    SVTYPE=DEL;END=20724;SVLEN=1108;NSAMPLES=85;OCC=2;FRQ=0.0235;CIPOS=0,0;CIEND=0,0;VARIANTS=|Diag-wgs00-19:10616:10724|Diag-wgs01-20:10616:10724 GT      0/1"

CHROMS = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "MT"]


def input_parser():
    parser = argparse.ArgumentParser(description='Remove VARIANT INFO field from SVDB VCF')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF-file made from SVDB db export')
    return parser.parse_args()


def parse(line):
    """
    Remove the VARIANTS INFO field that contains sample names
    """
    record, samples = utils.parse_record(line.strip())

    # Fix POS=0 from Canvas (should have been fixed in sv_standardizer)
    if record["pos"] == "0":
        record["pos"] = "1"
        _log.info(f"Fixed start POS for: {line}")

    if "VARIANTS" in record["info"]:
        variants = record["info"].pop("VARIANTS")
        _log.info(f"Removing variant names: {variants}")
    else:
        _log.warning(f"No INFO field VARIANTS in {line}")

    return utils.format_record(record, samples)


def main(file_object):
    with file_object:
        for line in file_object:
            if line.startswith("##"):
                print(line.strip())
            elif line.startswith("#CHROM"):
                print(line.strip())
            elif line.startswith(tuple(CHROMS)):
                print(parse(line))
            else:
                _log.warning("Dropping VCF line: {}".format(line))


if __name__ == "__main__":
    args = input_parser()
    logging.basicConfig(level=logging.DEBUG)
    if args.file:
        main(args.file)
