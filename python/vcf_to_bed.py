import argparse
import sys
import logging
_log = logging.getLogger(__name__)

# Expected VCF input on form:
# "1       20616   cluster_643     N       <DEL>   .       PASS    SVTYPE=DEL;END=20724;SVLEN=1108;NSAMPLES=85;OCC=2;FRQ=0.0235;CIPOS=0,0;CIEND=0,0;VARIANTS=|Diag-wgs00-19:10616:10724|Diag-wgs01-20:10616:10724 GT      0/1"

# BED output on form:
# #CHROM POS END SVTYPE FRQ STRAND POS END ITEMRGB

def input_parser():
    parser = argparse.ArgumentParser(description='SVDB VCF to BED')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF-file made from SVDB db export')
    return parser.parse_args()


def get_color(svtype):
    """
    Define colors for IGV
    * DEL is red = "255,0,0"
    * DUP is blue "0,0,255"'
    * Otherwise black
    """
    if svtype.startswith("DEL"):
        return "255,0,0"
    elif svtype.startswith("DUP"):
        return "0,0,255"
    else:
        return "0,0,0"


def parse(line):
    """
    Parse a VCF line and return a list of (identical) BED lines
    The number of BED lines are determined by the INFO field OCC
    The duplication is needed in order to make correct bigWig files
    """
    CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO = line.split()[:8]
    info = dict(map(lambda pair: pair.split("="), INFO.split(';')))

    SVTYPE = info["SVTYPE"]
    END = info.get("END", POS)
    SVLEN = info.get("SVLEN", "0").strip("-")

    color = get_color(SVTYPE)

    # A problem occurs when POS=1. Then the bed should have POS=0
    # Canvas does this automatically (violating the 1-indexness of the VCF)
    if POS == "1":
        POS_BED = "0"
    else:
        POS_BED = POS

    OCC = info.get("OCC", "1")

    variant = [CHROM, POS_BED, END, SVTYPE, info["FRQ"], "+", POS, END, color]
    return ["\t".join(variant).strip()] * int(OCC)


def main(file_object):
    with file_object:
        for line in file_object:
            if line and not line.startswith("#"):
                # BED lines are duplicated to get correct read depth
                # for the bigWig files
                for duplicated_line in parse(line):
                    print(duplicated_line)


if __name__ == "__main__":
    args = input_parser()
    logging.basicConfig(level=logging.DEBUG)
    if args.file:
        main(args.file)
