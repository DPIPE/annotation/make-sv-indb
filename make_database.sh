#!/bin/bash

# Stop script at first error
set -euf -o pipefail

# This script will make a SVDB database from VCF files in a directory.
#
# Depending on user input it will call one of the scripts
# scripts/make_database_by_singularity.{sh,slurm}
# These scripts call the
# runnable script bin/create-database inside a Singularity container
#
# User input is provided by a JSON file that has keys
# .indb_path: Path for INDB (path consists of $INDB_DIR/$INDB_NAME)
# .vcf_dir: Absolute path to folder containing VCFs
# .singularity: Absolute path to Singularity container that contains executable 'create-database'
# .version: Version of database (if empty string, use today's date)
# .executor: Set executor to 'local' [default] or 'slurm'
while [ "$#" -gt 0 ]; do
  case "$1" in

    --setup-file=*) SETUP_FILE="${1#*=}"; shift 1;;
    --setup-file) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "Unknown option: $1" >&2; exit 1;;
    *) "$1"; shift 1;;
  esac
done

if [ -f "$SETUP_FILE" ]; then
  INDB_PATH="$(jq -r '.indb_path' $SETUP_FILE)"
  VCF_DIR="$(jq -r '.vcf_dir' $SETUP_FILE)"
  VERSION="$(jq -r '.version' $SETUP_FILE)"
  SIMG="$(jq -r '.singularity' $SETUP_FILE)"
  EXECUTOR="$(jq -r '.executor' $SETUP_FILE)"
else
  >&2 echo "ERROR:make_database:$SETUP_FILE is not a file"
  exit 1
fi

# Set indb base name. Check existence of destination folder
INDB_DIR=$(realpath $(dirname $INDB_PATH))
INDB_NAME=$(basename $INDB_PATH)
if [ ! -d "$INDB_DIR" ]; then
  read -n1 -p "${INDB_DIR} is not a directory. Create? [yN]" doit 
  case $doit in  
    y|Y) mkdir -p ${INDB_DIR} ;; 
    *) >&2 echo "ERROR:make_database:Terminated due to missing directory ${INDB_DIR}"; exit 1 ;; 
  esac
else
  >&2 echo "INFO:make_database:Set database base name INDB_PATH=${INDB_DIR}/${INDB_NAME}"
fi

# Optionally set database version to today's date
if [ -z "${VERSION:-}" ]; then
  printf -v DATE '%(%Y%m%d)T' -1
  VERSION=${DATE}
  >&2 echo "WARNING:make_database:Automatically set VERSION=${DATE}"
fi

# Make database
if [ "$EXECUTOR" = "slurm" ]; then
  sbatch scripts/make_database_by_singularity.slurm \
    ${INDB_NAME} \
    ${INDB_DIR} \
    ${VCF_DIR} \
    ${VERSION} \
    ${SIMG}
else
  ./scripts/make_database_by_singularity.sh \
    ${INDB_NAME} \
    ${INDB_DIR} \
    ${VCF_DIR} \
    ${VERSION} \
    ${SIMG}
fi
