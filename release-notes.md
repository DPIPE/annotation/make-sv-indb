# Release notes

## Version v1.0.4

* Add filtering to *standardize_sample.sh*. Filter defined in `interpretation_group` in the config files
* Different sample choice for manta and canvas because SVDB does not scale sufficiently for manta.

## Version v1.0.3

* Updated Singularity container to `v1-03`
* Updated `sv-pack` to version `v1.3.0`
* Allow Verso 2D sample ids and cardio sample ids

## Version v1.0.2

* Updated Singularity container to `v1-02`
* Added anonymized Ella VCF track

## Version v1.0.1

* Updated Singularity container to `v1-01`
* Added colour to Ella tracks
* Fixed error in procedure for slurm executor

## Version v1.0.0

Initial version
