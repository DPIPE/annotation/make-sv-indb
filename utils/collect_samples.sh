#!/usr/bin/env bash

# Stop script at first error
set -euf -o pipefail

# Get setup JSON file that has keys
# .pattern.glob: Glob pattern for fast search for relevant VCFs
# .pattern.regex: Regex pattern to pick out a subset of the VCFs
# .vcf_transfer_dir: Destination folder
while [ "$#" -gt 0 ]; do
  case "$1" in

    --setup-file=*) SETUP_FILE="${1#*=}"; shift 1;;
    --dryrun*) DRYRUN="True"; shift 1;;
    --setup-file) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "Unknown option: $1" >&2; exit 1;;
    *) "$1"; shift 1;;
  esac
done

if [ -f $SETUP_FILE ]; then
  GLOB_PATTERN="$(jq -r '.pattern.glob' $SETUP_FILE)"
  REGEX_PATTERN="$(jq -r '.pattern.regex' $SETUP_FILE)"
  VCF_TRANSFER_DIR="$(jq -r '.vcf_transfer_dir' $SETUP_FILE)"
else
  >&2 echo "ERROR:collect_samples:$SETUP_FILE is not a file"
  exit 1
fi

# Check existence of destination
if [ ! -d "$VCF_TRANSFER_DIR" ]; then
  read -n1 -p "${VCF_TRANSFER_DIR} is not a directory. Create? [yN]" doit 
  case $doit in  
    y|Y) mkdir -p ${VCF_TRANSFER_DIR} ;; 
    *) >&2 echo "ERROR:collect_samples:Terminated due to missing directory ${VCF_TRANSFER_DIR}"; exit 1 ;; 
  esac
else
  >&2 echo "INFO:collect_samples:Copy VCFs to ${VCF_TRANSFER_DIR}"
fi

# Find file-paths and store them in a file
# in $VCF_TRANSFER_DIR/log/paths_${DATE}.log
mkdir -p ${VCF_TRANSFER_DIR}/log
printf -v DATE '%(%Y%m%d)T' -1
set +o noglob
ls -1U $GLOB_PATTERN > ${VCF_TRANSFER_DIR}/log/paths_${DATE}.log
set -o noglob

# Select files from glob pattern
declare -a FILES=( $(grep -P ${REGEX_PATTERN} ${VCF_TRANSFER_DIR}/log/paths_${DATE}.log | paste -s -d ' ' -) )

# [Optional] Dry-run rsync
DRYRUN="${DRYRUN:-False}"
if [ "$DRYRUN" = "True" ]; then
  RSYNC_PARAMS="-avnP"
else
  RSYNC_PARAMS="-avP"
fi

for FILE in "${FILES[@]}"; do
  if [ -f "$FILE" ] && [ -n "$FILE" ]; then
   CMD="ionice rsync $RSYNC_PARAMS $FILE $VCF_TRANSFER_DIR"
   >&2 echo "INFO:collect_samples:$CMD"
   $CMD
  else
    >&2 echo "WARNING:collect_samples:$FILE does not exist"
  fi
  sleep 0.01
done