#!/usr/bin/env bash

# Stop script at first error
set -euf -o pipefail

# Store database in Git for hashed versioning
# Only one version of the database should exist at
# a particular time. Previous versions are identified by
# av version-tag

# Get setup JSON file that has keys
# .indb_path: Path for INDB (path consists of $INDB_DIR/$INDB_NAME)
# .version: Version of database (if empty, a database with today's date must exist)
# .procedure_version: Version of database making procedure
# .singularity: Singularity image containing git
while [ "$#" -gt 0 ]; do
  case "$1" in

    --setup-file=*) SETUP_FILE="${1#*=}"; shift 1;;
    --release) FINALIZE_RELEASE='True'; shift 1;;
    --setup-file) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "Unknown option: $1" >&2; exit 1;;
    *) "$1"; shift 1;;
  esac
done

FINALIZE_RELEASE=${FINALIZE_RELEASE:-"False"}
>&2 echo "INFO:store_database:Running with FINALISE_RELEASE=${FINALIZE_RELEASE}"

if [ -f "$SETUP_FILE" ]; then
  INDB_PATH="$(jq -r '.indb_path' $SETUP_FILE)"
  VERSION="$(jq -r '.version' $SETUP_FILE)"
  PROCEDURE_VERSION="$(jq -r '.procedure_version' $SETUP_FILE)"
  SIMG="$(jq -r '.singularity' $SETUP_FILE)"
else
  >&2 echo "ERROR:standardize_samples:$SETUP_FILE is not a file"
  exit 1
fi

# Optionally set database version to today's date
if [ -z "${VERSION:-}" ]; then
  printf -v DATE '%(%Y%m%d)T' -1
  VERSION=${DATE}
  >&2 echo "WARNING:store_database:Automatically infer VERSION=${DATE}"
fi

# Resolve filpath, check existence
>&2 echo "INFO:store_database:Resolving absolute path of INDB_PATH=${INDB_PATH}"
INDB_PATH=$(realpath $INDB_PATH)
>&2 echo "INFO:store_database:Get baseneame of INDB_PATH=${INDB_PATH}"
INDB_BASENAME=$(basename $INDB_PATH)
>&2 echo "INFO:store_database:Resolving directory of INDB_PATH=${INDB_PATH}"
INDB_DIR=$(dirname $INDB_PATH)
>&2 echo "INFO:store_database:Resolving filepath INDB_VCF_GZ=${INDB_PATH}_${VERSION}.vcf.gz"
INDB_VCF_GZ=$(realpath -e "${INDB_PATH}_${VERSION}.vcf.gz")
>&2 echo "INFO:store_database:Resolving filepath INDB_VCF_TBI=${INDB_PATH}_${VERSION}.vcf.gz.tbi"
INDB_VCF_TBI=$(realpath -e "${INDB_PATH}_${VERSION}.vcf.gz.tbi")
>&2 echo "INFO:store_database:Resolving filepath INDB_SQLITE=${INDB_PATH}_${VERSION}.db"
INDB_SQLITE=$(realpath -e "${INDB_PATH}_${VERSION}.db")
>&2 echo "INFO:store_database:Resolving path of SIMG=${SIMG}"
SIMG=$(realpath -e "${SIMG}")

CWD=$(pwd)
>&2 echo "INFO:store_database:Entering directory ${INDB_DIR}"
cd $INDB_DIR

# Run in correct environment
SINGULARITY="singularity exec -H $INDB_DIR $SIMG"

# Ensure the existence of a git repo with git tracking
if [ ! -d ".git" ]; then
  $SINGULARITY git init
  $SINGULARITY git config user.email "maintainer@amg.ous"
  $SINGULARITY git config user.name "Maintainer"
  $SINGULARITY git checkout -b master
  >README.md echo 'To deploy: Move and `tar xvf GIT_ARCHIVE` in `sensitive-db/`'
  $SINGULARITY git add README.md
  $SINGULARITY git commit -m "[wgs-sv] Init repo"
fi

# Make release branch
RELEASE_VERSION="$PROCEDURE_VERSION-$VERSION"
>&2 echo "INFO:store_database:Commit database to release branch release-$RELEASE_VERSION"
# Check if release-$VERSION already exists
if $SINGULARITY git rev-parse release-$RELEASE_VERSION >/dev/null 2>&1; then
  $SINGULARITY git checkout release-$RELEASE_VERSION
else
  >&2 echo "INFO:store_database:Make new release branch release-$RELEASE_VERSION"
  $SINGULARITY git checkout -b release-$RELEASE_VERSION master
fi

# Add databases
$SINGULARITY git add $INDB_VCF_GZ $INDB_VCF_TBI
if [[ "$(git diff --cached --exit-code)" ]]; then
  $SINGULARITY git commit -m "INDB-SV: Add $INDB_BASENAME to release $RELEASE_VERSION"
else
  >&2 echo "WARNING:store_database:$INDB_VCF_GZ $INDB_VCF_TBI are already in release-$RELEASE_VERSION"
fi

# Publish new version
echo "Version $RELEASE_VERSION of the sv-indbs now contains the following files"
$SINGULARITY git ls-tree -r --name-only release-$RELEASE_VERSION
read -n1 -p "Do you want to release version $RELEASE_VERSION? [yN]" doit 
case $doit in  
  y|Y) CREATE="True" ;; 
  *) >&2 echo "INFO:store_database:Release $RELEASE_VERSION expects more databases" ;; 
esac

# Default to CREATE=False
CREATE="${CREATE:-False}"

if [ "$CREATE" = "True" ]; then
  ARCHIVE_NAME="sv-wgs-${RELEASE_VERSION}.tar.gz"
  >README.md echo 'To deploy: Move and `tar xvf '"${ARCHIVE_NAME}"'` in `sensitive-db/`'
  $SINGULARITY git add README.md
  $SINGULARITY git commit -m "INDB-SV: Update README for $RELEASE_VERSION"

  >&2 echo "INFO:store_database:Add tag $RELEASE_VERSION"
  $SINGULARITY git tag -a $RELEASE_VERSION -m "$RELEASE_VERSION"
  >&2 echo "INFO:store_database:Successfully added $RELEASE_VERSION to releases:"
  >&2 echo "$($SINGULARITY git tag -l)"

  $SINGULARITY git archive -v --prefix wgs-sv/ --output $ARCHIVE_NAME $RELEASE_VERSION
  >&2 echo "INFO:store_database:Successfully made release archive ${ARCHIVE_NAME}"
fi

# Do manual checking.
echo "Current status of repository is:"
$SINGULARITY git ls-tree -r release-$RELEASE_VERSION
$SINGULARITY git --no-pager log --graph --pretty=oneline

cd $CWD
>&2 echo "INFO:store_database:Successfully returning to ${CWD}"