#!/usr/bin/env bash

# Stop script at first error
set -euf -o pipefail

# Directory of this script
THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Postprocess SVDB databases
# * Remove decoys and superfluous sample columns ("flatten" database)
# * Compress and index the resulting file

# Get setup JSON file that has keys
# .indb_path: Path for INDB (path consists of $INDB_DIR/$INDB_NAME)
# .version: Version of database (if empty, a database with today's date must exist)
# .singularity: Singularity image containing svdb
while [ "$#" -gt 0 ]; do
  case "$1" in

    --setup-file=*) SETUP_FILE="${1#*=}"; shift 1;;
    --setup-file) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "Unknown option: $1" >&2; exit 1;;
    *) "$1"; shift 1;;
  esac
done


if [ -f "$SETUP_FILE" ]; then
  INDB_PATH="$(jq -r '.indb_path' $SETUP_FILE)"
  VERSION="$(jq -r '.version' $SETUP_FILE)"
  SIMG="$(jq -r '.singularity' $SETUP_FILE)"
else
  >&2 echo "ERROR:postprocess_databases:$SETUP_FILE is not a file"
  exit 1
fi

# Optionally set database version to today's date
if [ -z "${VERSION:-}" ]; then
  printf -v DATE '%(%Y%m%d)T' -1
  VERSION=${DATE}
  >&2 echo "WARNING:postprocess_databases:Automatically infer VERSION=${DATE}"
fi

# In the following we will check for the existence of the following file
INDB_VCF=${INDB_PATH}_${VERSION}.vcf

# Resolve filpath, check existence
>&2 echo "INFO:postprocess_databases:Resolving absolute path of INDB_VCF=${INDB_VCF}"
INDB_VCF=$(realpath -e $INDB_VCF)
>&2 echo "INFO:postprocess_databases:Resolving directory of INDB_VCF=${INDB_VCF}"
INDB_DIR=$(dirname $INDB_VCF)
>&2 echo "INFO:postprocess_databases:Resolving path of SIMG=${SIMG}"
SIMG=$(realpath -e "${SIMG}")

# Backup original VCF file
ORIGINAL_VCF=${INDB_PATH}_${VERSION}_orig.vcf
ORIGINAL_VCF=$(realpath $ORIGINAL_VCF)

>&2 echo "INFO:postprocess_databases:Rename database $INDB_VCF -> $ORIGINAL_VCF"
mv -i $INDB_VCF $ORIGINAL_VCF

CMD="bash ${THISDIR}/flatten_svdb.local $SIMG ${ORIGINAL_VCF}"
>&2 echo "INFO:postprocess_databases:Flatten database ${CMD} > ${INDB_VCF}"
$CMD > "${INDB_VCF}"

N_ORIG=$(grep -c '^[#1-9XYM]' ${ORIGINAL_VCF} || [[ $? -eq 1 ]])
N_FLAT=$(grep -c '^[#1-9XYM]' ${INDB_VCF} || [[ $? -eq 1 ]])

if [[ ${N_ORIG} -ne ${N_FLAT} ]]; then
    >&2 echo "ERROR:postprocess_database:Flattening changed number of variants"
    exit 1
fi

CMD="bash ${THISDIR}/compress_svdb.local $SIMG ${INDB_VCF}"
>&2 echo "INFO:postprocess_databases:Compress database ${CMD}"
$CMD

>&2 echo "INFO:postprocess_databases:Successfully ending postprocess_databases.sh"