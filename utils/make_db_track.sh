#!/usr/bin/env bash

# Stop script at first error
set -euf -o pipefail

# Directory of this script
THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Present database as bigWig-file

# Get setup JSON file that has keys
# .indb_path: Path for INDB (path consists of $INDB_DIR/$INDB_NAME)
# .version: Version of database (if empty, a database with today's date must exist)
# .singularity: Singularity image containing 'anonymize_vcf', bgzip and tabix 
# .executor: Set executor to 'local' [default] or 'slurm'
while [ "$#" -gt 0 ]; do
  case "$1" in

    --setup-file=*) SETUP_FILE="${1#*=}"; shift 1;;
    --setup-file) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "Unknown option: $1" >&2; exit 1;;
    *) "$1"; shift 1;;
  esac
done


if [ -f "$SETUP_FILE" ]; then
  INDB_PATH="$(jq -r '.indb_path' $SETUP_FILE)"
  VERSION="$(jq -r '.version' $SETUP_FILE)"
  SIMG="$(jq -r '.singularity' $SETUP_FILE)"
  EXECUTOR="$(jq -r '.executor' $SETUP_FILE)"
else
  >&2 echo "ERROR:standardize_samples:$SETUP_FILE is not a file"
  exit 1
fi

# Optionally set database version to today's date
if [ -z "${VERSION:-}" ]; then
  printf -v DATE '%(%Y%m%d)T' -1
  VERSION=${DATE}
  >&2 echo "WARNING:make_db_track:Automatically infer VERSION=${DATE}"
fi

# Resolve filpath, check existence
>&2 echo "INFO:make_db_track:Resolving absolute path of INDB_PATH=${INDB_PATH}"
INDB_PATH=$(realpath $INDB_PATH)
>&2 echo "INFO:make_db_track:Get baseneame of INDB_PATH=${INDB_PATH}"
INDB_BASENAME=$(basename $INDB_PATH)
>&2 echo "INFO:make_db_track:Resolving directory of INDB_PATH=${INDB_PATH}"
INDB_DIR=$(dirname $INDB_PATH)
>&2 echo "INFO:make_db_track:Resolving filepath INDB_VCF=${INDB_PATH}_${VERSION}.vcf"
INDB_VCF=$(realpath -e "${INDB_PATH}_${VERSION}.vcf")
>&2 echo "INFO:make_db_track:Resolving path of SIMG=${SIMG}"
SIMG=$(realpath -e "${SIMG}")

if [ "$EXECUTOR" = "slurm" ]; then
  sbatch $THISDIR/make_db_track.slurm $INDB_VCF $SIMG
else
  bash $THISDIR/make_db_track.local $INDB_VCF $SIMG
fi

>&2 echo "INFO:make_db_track:Successfully ending make_db_track.sh"