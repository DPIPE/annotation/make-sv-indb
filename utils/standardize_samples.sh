#!/usr/bin/env bash

# Stop script at first error
set -euf -o pipefail

# Standardize VCF-files by sv_standardizer from sv-pack
# Will standardize all VCF-files found in ".vcf_dir":

# Get setup JSON file that has keys
# .vcf_transfer_dir: Folder that contains VCF-files
# .vcf_dir: Destination folder for standardized files
# .caller: One of [manta,cnvnator,canvas,delly,tiddit,svaba,merged]
# .singularity: Singularity image containing executable sv_standardizer
while [ "$#" -gt 0 ]; do
  case "$1" in

    --setup-file=*) SETUP_FILE="${1#*=}"; shift 1;;
    --setup-file) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "Unknown option: $1" >&2; exit 1;;
    *) "$1"; shift 1;;
  esac
done

if [ -f "$SETUP_FILE" ]; then
  VCF_TRANSFER_DIR="$(jq -r '.vcf_transfer_dir' $SETUP_FILE)"
  VCF_DIR="$(jq -r '.vcf_dir' $SETUP_FILE)"
  CALLER="$(jq -r '.caller' $SETUP_FILE)"
  SIMG="$(jq -r '.singularity' $SETUP_FILE)"
  INTERPRETATION_GROUP="$(jq -r '.interpretation_group' $SETUP_FILE)"
else
  >&2 echo "ERROR:standardize_samples:$SETUP_FILE is not a file"
  exit 1
fi

# Find files from glob pattern
set +o noglob
declare -a FILES=( $(ls -1U ${VCF_TRANSFER_DIR}/*.vcf | paste -s -d ' ' -) )
set -o noglob

# Check existence of destination
if [ ! -d "$VCF_DIR" ]; then
  read -n1 -p "${VCF_DIR} is not a directory. Create? [yN]" doit 
  case $doit in  
    y|Y) mkdir -p ${VCF_DIR} ;; 
    *) >&2 echo "ERROR:standardize_samples:Terminated due to missing directory ${VCF_DIR}"; exit 1 ;; 
  esac
else
  >&2 echo "INFO:standardize_samples:Put standardized VCFs in ${VCF_DIR}"
fi

for FILE in "${FILES[@]}"; do
  if [ -f "$FILE" ] && [[ $FILE == *.vcf ]]; then
    ABS_FILE="$(realpath -e ${FILE})"
    BASENAME="$(basename ${ABS_FILE} .vcf)"
    BASEDIR="$(dirname ${ABS_FILE})"
    CMD="singularity exec -H ${BASEDIR} $SIMG sv_standardizer --caller ${CALLER} ${ABS_FILE}"
    >&2 echo "INFO:standardize_samples:$CMD"
    $CMD > "${VCF_DIR}/${CALLER}_${BASENAME}_std.vcf"
    >&2 echo "INFO:standardize_samples:Remove all variants not in ${INTERPRETATION_GROUP} by ${CMD}"
    CMD="singularity exec -H ${VCF_DIR} $SIMG sv_wgs_filtering"
    $CMD \
      "${VCF_DIR}/${CALLER}_${BASENAME}_std.vcf" \
      --set-interpretation-group "${INTERPRETATION_GROUP}" \
      --caller-priority ${CALLER} \
      --output-format vcf >tmp_cnv.vcf
    mv tmp_cnv.vcf "${VCF_DIR}/${CALLER}_${BASENAME}_std.vcf"
  else
    >&2 echo "WARNING:standardize_samples:$FILE is not a VCF-file"
  fi
  sleep 0.01
done
>&2 echo "INFO:standardize_samples:Standardization complete"