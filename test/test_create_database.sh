#!/bin/bash

# Test script for use in CI-test

INDB_PATH=/out/test/indb_db_test
echo "INDB_PATH=${INDB_PATH}"

/bin/bash create-database /vcf ${INDB_PATH}

if [[ ! -f "${INDB_PATH}.vcf" ]]; then
    echo "ERROR:File ${INDB_PATH}.vcf does not exist"
    exit 1
fi
if [[ ! -f "${INDB_PATH}.db" ]]; then
    echo "ERROR:File ${INDB_PATH}.db does not exist"
    exit 1
fi
echo "test_create_database.sh was OK!"