BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
SIMG_TAG ?= $(BRANCH)
PROCEDURE_VERSION ?= $(BRANCH)
PROJECT ?= $(shell basename "${PWD}")
PROJECT_DIR ?= $(shell pwd)
IMAGE_TAG ?= $(PROJECT):$(SIMG_TAG)
SETUP_FILE ?= test/config/test.json


build-docker:
	docker build --pull -t $(IMAGE_TAG) .

build-singularity:
	mkdir -p singularity
	singularity build singularity/$(PROJECT)-$(SIMG_TAG).sif \
	docker-daemon://$(IMAGE_TAG)

.PHONY: build
build: build-docker build-singularity

git-archive:
	mkdir -p archive
	git archive -o archive/$(PROJECT)-$(PROCEDURE_VERSION).zip HEAD

singularity-shell:
	singularity shell singularity/$(PROJECT)-$(SIMG_TAG).sif

docker-shell:
	docker run -it $(IMAGE_TAG)

transfer-dryrun:
	./utils/collect_samples.sh --setup-file=$(SETUP_FILE) --dryrun

.PHONY: transfer
transfer:
	./utils/collect_samples.sh --setup-file=$(SETUP_FILE)

.PHONY: standardize
standardize:
	./utils/standardize_samples.sh --setup-file=$(SETUP_FILE)

.PHONY: database
database:
	./make_database.sh --setup-file=$(SETUP_FILE)

.PHONY: postprocess
postprocess:
	./utils/postprocess_database.sh --setup-file=$(SETUP_FILE)

.PHONY: db-track
db-track:
	./utils/make_db_track.sh --setup-file=$(SETUP_FILE)

.PHONY: merge
merge:
	./utils/merge_databases.sh --setup-file=$(SETUP_FILE)

.PHONY: store-database
store-database:
	./utils/store_database.sh --setup-file=$(SETUP_FILE)

.PHONY: full-process
full-process: transfer standardize database

.PHONY: test
test:
	docker run -v $(PROJECT_DIR):/out -v $(PROJECT_DIR)/test/testdata:/vcf $(IMAGE_TAG) /work/test/test_create_database.sh

.PHONY: clean
clean:
	rm -rI singularity archive