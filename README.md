# Make SVDB database

Make frequency databases for structural variants by [SVDB](https://github.com/J35P312/SVDB)
The databases will be used for removing variants with high frequency from patient samples

First check that the following holds (see below for details)

* Prerequisite 1: The correct singularity container exists in TSD
* Prerequisite 2: The correct version of this repo has been transferred to TSD
* Prerequisite 3: The bash-environment contains `singularity/3.7.3` and `jq`

## Version v1.0.3

This version contains database definition files for

* Dragen manta: `manta_dr.json`
* Dragen canvas: `canvas_dr.json`

Full procedure:

```bash
#####################################################
# Data verification step:
# Verify that data can be transferred to cluster
#####################################################
SETUP_FILE=config/manta_dr.json make transfer-dryrun
SETUP_FILE=config/canvas_dr.json make transfer-dryrun
#####################################################
# Perform the following steps
# Data preparation step 1: Transfer data to cluster
# Data preparation step 2: Standardize VCFs
# Make database
#####################################################
SETUP_FILE=config/manta_dr.json make full-process
SETUP_FILE=config/canvas_dr.json make full-process
#####################################################
# Wait until the databases have been created
# Postprocess database (compression and indexing)
#####################################################
SETUP_FILE=config/manta_dr.json make postprocess
SETUP_FILE=config/canvas_dr.json make postprocess
```

# The steps in detail

## Prerequisite 1: Make Singularity container

A compatible singularity container can be made by

```bash
SIMG_TAG=v1-03 make build
```

This creates an intermediary Docker image and a Singularity container that contain this repo.
They are stored in

* `singularity/make-sv-indb-${SIMG_TAG}.sif`: Singularity container

Manually transfer the singularity container to

```bash
/cluster/projects/p22/singularity/make-sv-indb/make-sv-indb-${SIMG_TAG}.sif
```

## Prerequisite 2: Move release of `make-sv-indb` to TSD

This repo can be transferred into TSD as a git archive. Check out current release of `make-sv-indb` and make git archive of current `HEAD` as `PROCEDURE_VERSION`

```bash
# git checkout tags/v1.0.4 -b release-tsd-v104
PROCEDURE_VERSION=v1.0.4 make git-archive
```

Manually transfer and `unzip` the archive inside

```bash
mkdir -p /cluster/projects/p22/dev/shared/sv-indb/make-sv-indb
```

Add archive to local git archive

```bash
cd /cluster/projects/p22/dev/shared/sv-indb/make-sv-indb
unzip make-sv-indb-${PROCEDURE_VERSION}.zip
git commit -am "Extracted version ${PROCEDURE_VERSION}"
```

## Data preparation 1: Transfer CNV VCFs to cluster

VCFs can be transfered from TSD durable to a cluster `VCF_TRANSFER_DIR` in two steps:

```bash
SETUP_FILE=SETUP_COLLECT_FILE make transfer-dryrun
SETUP_FILE=SETUP_COLLECT_FILE make transfer
```

Here the `SETUP_COLLECT_FILE` JSON file has mandatory keys

```json
{
    "pattern": {
        "glob": GLOB_PATTERN    # Glob pattern to locate all relevant
                                # VCFs by `ls -1U`
                                # Keep output in VCF_DIR/log/paths_DATE.log
        "regex": REGEX_PATTERN  # Regex pattern to chose a subset of the files
    }
    "vcf_transfer_dir": VCF_TRANSFER_DIR # Directory to put all relevant VCFs
}
```

## Data preparation 2: Standardizing VCFs

We expect that CNVs have to be standardized to a format that can be handled by the
downstream pipeline.

For each database, this can be done by

```bash
SETUP_FILE=SETUP_STANDARDIZE_FILE make standardize
```

where the `SETUP_STANDARDIZE_FILE` JSON file has keys

```json
{
    "vcf_transfer_dir": VCF_TRANSFER_DIR # The destination of the non-standardized VCFs
    "vcf_dir": VCF_DIR            # The destination of the standardized VCFs
    "caller": CALLER              # Standardization is caller depent.
                                  # CALLER has meaning for values
                                  # `manta`, `canvas`, `cnvnator`, `delly`, `tiddit`, `svaba`
    "interpretation_group": JSON  # As defined in the `sv_wgs_filtering` 
                                  # in svpack
    "singularity": SIMG           # Path to a Singularity container
                                  # with executable script `sv_standardizer`
}
```

## Make database

Make a SVDB database for frequency annotation of VCFs containing CNVs

```bash
SETUP_FILE=SETUP_DB_FILE make database
```

The `SETUP_DB_FILE` JSON file contains mandatory keys

```json
{
    "vcf_dir": VCF_DIR      # Name of directory containing standardized VCFs
    "indb_path": INDB_PATH  # Base name of the database including absolute directory
    "version": VERSION      # Database version. If empty string,
                            # today's date is automatically added
    "singularity": SIMG     # Name of singularity container with `svdb` and `vcftools`.
                            # $VCF_DIR is bound to the directory 
                            # `/vcf` inside the container. 
                            # `bin`-folder expected in $PATH inside
                            # the container.
    "executor": ...         # Use either 'local' [default] to run 
                            # `./scripts/*.sh`
                            # or 'slurm' to run `batch scripts/*.slurm`
}
```

Two database files are created:

* `${INDB_PATH}_$VERSION.vcf`: VCF file
* `${INDB_PATH}_$VERSION.db`: SQLite file

## Postprocess database

```bash
SETUP_FILE=SETUP_DB_FILE make postprocess
```

The `SETUP_DB_FILE` JSON file contains mandatory keys

```json
{
    "indb_path": INDB_PATH  # Base name of the database including absolute directory
    "version": VERSION      # Database version. If empty string,
                            # today's date is automatically used
    "singularity": SIMG     # Name of singularity container with 
                            # `normalize_vcf`, `bgzip` and `tabix`
}
```

Two new database files are created:

* `${INDB_PATH}_$VERSION.vcf.gz`: bgzip-ed VCF with just one SAMPLE column and no decoy contigs
* `${INDB_PATH}_$VERSION.vcf.gz.tbi`: tabix-ed VCF

The original database file is renamed `${INDB_PATH}_${VERSION}.vcf -> ${INDB_PATH}_${VERSION}_orig.vcf`

# Merge SVDBs

Merge VCFs by SVDB
_This procedure can be performed on VCFs that can be read by the SVDB tool_

_Prior to merging it is recommended to run the step `make postprocess`_

This procedure will merge all files with file pattern
`*_$VERSION.vcf.gz` that are present in the directory of `INDB_PATH`
when run as

```bash
SETUP_FILE=SETUP_MERGE make merge
```

where `SETUP_MERGE` contains

```json
{
    "indb_path": INDB_PATH  # Base name of the database, including 
                            # the absolute directory
    "version": VERSION      # Database version. If empty string,
                            # today's date is automatically used
    "singularity": SIMG     # Name of singularity container with SVDB
    "executor": EXECUTOR    # # Use either 'local' [default] to run 
                            # `./utils/merge_databases.local`
                            # or 'slurm' to run `sbatch utils/merge_databases.slurm`
}
```

# Make database-track from database

Make a anonymous representation of a CNV database
The VCF-file can be shown as a track in IGV
_This procedure can be performed on any SVDB-VCF, e.g. from  SweGen_

For each SVDB-VCF run

```bash
SETUP_FILE=SETUP_DB_TRACK make db-track
```

where `SETUP_DB_TRACK` contains

```json
{
    "indb_path": INDB_PATH  # Base name of the database, including 
                            # the absolute directory
    "version": VERSION      # Database version. If empty string,
                            # today's date is automatically added
    "singularity": SIMG     # Name of singularity container with 
                            # `annonymize_vcf`, `bgzip` and `tabix`.
                            # Bind dirname of INDB_PATH to HOME-folder
    "executor": EXECUTOR    # # Use either 'local' [default] to run 
                            # `./utils/make_db_track.local`
                            # or 'slurm' to run `sbatch utils/make_db_track.slurm`
}
```

# Utility function that is used before wgs-sv becomes part of sensitive-db

## Keep track of database version by `git`

In order to keep track of concurrent versions of the databases
during development and creation of git archies for deployment,
each caller specific database is identified by

* `git tag ${PROCEDURE_VERSION}-${VERSION}` 
* `git branch release-${PROCEDURE_VERSION}-${VERSION}`
* `sv-wgs-${PROCEDURE_VERSION}-${VERSION}.tar.gz`

```bash
#####################################################
# A 'release' contains all databases that are relevant
# for a particular context (e.g. ELLA-Dragen, Statistics-GATK)
# When prompted, make sure to wait with the release 
# until all databases have been added
#####################################################
SETUP_FILE=config/manta_dr.json make store-database
SETUP_FILE=config/canvas_dr.json make store-database
#####################################################
# Any errors in the 'release' of a collection of databases
# must be solved manually by `git`-tricks
```

For each database run

```bash
SETUP_FILE=SETUP_STORAGE make store-database
```

where `SETUP_STORAGE` contains

```json
{
    "indb_path": INDB_PATH  # Base name of the database, including 
                            # the absolute directory
    "version": VERSION      # Database version. If empty string,
                            # today's date is automatically added
    "procedure_version": \
       PROCEDURE_VERSION    # Version of 'make-sv-indb'
    "singularity": SIMG     # Name of singularity container with `git`.
                            # Bind dirname of INDB_PATH to HOME-folder
}
```

# Testing

## Local testing

Manually verify that we can make

```bash
# After local run of `SIMG_TAG=v1-03 make build`
# Test that we have a working Docker image
# `SIMG_TAG=v1-03 make test`
# Add one database at the time. Release when all are added
SETUP_FILE=test/config/test1.json make full-process postprocess
SETUP_FILE=test/config/test2.json make full-process postprocess
# Merge databases
SETUP_FILE=test/config/test_merged.json make merge
# Make anonymous database track
SETUP_FILE=test/config/test_merged.json make db-track
```

On date `DATE` expect the following output of `tree -J test/tmp`

```json
[
  {"type":"directory","name":"test/tmp","contents":[
    {"type":"directory","name":"log","contents":[
      {"type":"file","name":"paths_${DATE}.log"}
    ]},
    {"type":"directory","name":"std","contents":[
      {"type":"file","name":"cnvnator_test-wgs00-2345_std.vcf"},
      {"type":"file","name":"cnvnator_test-wgs88-1234-MK_std.vcf"},
      {"type":"file","name":"cnvnator_test-wgs88-HG1234-FM_std.vcf"}
    ]},
    {"type":"directory","name":"svdb","contents":[
      {"type":"file","name":"indb_dr_merged_${DATE}.vcf"},
      {"type":"file","name":"indb_dr_merged_${DATE}_track.vcf.gz"},
      {"type":"file","name":"indb_dr_merged_${DATE}_track.vcf.gz.tbi"},
      {"type":"file","name":"indb_dr_test1_${DATE}.db"},
      {"type":"file","name":"indb_dr_test1_${DATE}.vcf.gz"},
      {"type":"file","name":"indb_dr_test1_${DATE}.vcf.gz.tbi"},
      {"type":"file","name":"indb_dr_test1_${DATE}_orig.vcf"},
      {"type":"file","name":"indb_dr_test2_${DATE}.db"},
      {"type":"file","name":"indb_dr_test2_${DATE}.vcf.gz"},
      {"type":"file","name":"indb_dr_test2_${DATE}.vcf.gz.tbi"},
      {"type":"file","name":"indb_dr_test2_${DATE}_orig.vcf"}
    ]},
    {"type":"file","name":"test-wgs00-2345.vcf"},
    {"type":"file","name":"test-wgs88-1234-MK.vcf"},
    {"type":"file","name":"test-wgs88-HG1234-FM.vcf"}
  ]},
  {"type":"report","directories":3,"files":18}
]
```

## Testing on TSD with real data

```bash
# Test on smaller dataset in TSD
# * Tests that slurm works
# * Tests that singularity container exists
SETUP_FILE=test/config/test_slurm_tsd.json make transfer-dryrun
SETUP_FILE=test/config/test_slurm_tsd.json make full-process
# Wait untill SLURM has returned results from previous process
SETUP_FILE=test/config/test_slurm_tsd.json make postprocess
# Merge databases
SETUP_FILE=test/config/test_merged_tsd.json make merge
# Wait untill SLURM has finished. Make database track
SETUP_FILE=test/config/test_merged_tsd.json make db-track
# Test storage of final database
SETUP_FILE=test/config/test_slurm_tsd.json make store-database
```

## CI-test

A CI-test for the Docker image can be run manually when doing merge requests
It consists of `make build-docker && make test`
