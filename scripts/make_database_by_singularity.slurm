#!/bin/bash
#SBATCH --job-name=MakeSvdb --account=p22_tsd
#SBATCH --time=500:00:00 --mem-per-cpu=7G --cpus-per-task=9

source /cluster/bin/jobsetup
module purge   # clear any inherited modules
module load singularity/3.7.3 # import singularity
set -o errexit # exit on errors

# Slurm executor. 
# Run 'create-database' from singularity image SIMG on slurm-cluster
# This script should be invoked by 'make_database.sh'

INDB_NAME="${1}" # Filename base for database
INDB_DIR="${2}" # Directory for database
VCF_DIR="${3}" # Absolute path to folder containing VCFs
VERSION="${4}" # Version of database. Determined in 'make_database.sh'
SIMG="${5}" # Absolute path to Singularity container that contains executable 'create-database'

## Mark outfiles for automatic copying to given path:
if [ -d "$INDB_DIR" ]; then
  cleanup "cp ${SCRATCH}/${INDB_NAME}.db ${INDB_DIR}/${INDB_NAME}_${VERSION}.db"
  cleanup "cp ${SCRATCH}/${INDB_NAME}.vcf ${INDB_DIR}/${INDB_NAME}_${VERSION}.vcf"
else
  >&2 echo "ERROR:make_database:Check that $INDB_DIR is a directory"
  exit 1
fi

if [ -d "$VCF_DIR" ] && [ -n "$INDB_NAME" ]; then
  >&2 echo "INFO:make_database:Bind $VCF_DIR to /vcf and build database"
  singularity exec -H $SCRATCH -B ${VCF_DIR}:/vcf $SIMG create-database \
    /vcf \
    ${SCRATCH}/${INDB_NAME}
else
  >&2 echo "ERROR:make_database:Check that $VCF_DIR is a directory"
  >&2 echo "ERROR:make_database:Check that $INDB_NAME is not empty"
  exit 1
fi
