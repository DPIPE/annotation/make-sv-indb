#!/bin/bash

# Stop script at first error
set -euf -o pipefail

# Shell executor.
# Run 'create-database' from singularity image SIMG
# This script should be invoked by 'make_database.sh'

INDB_NAME="${1}" # Filename base for database
INDB_DIR="${2}" # Directory for database
VCF_DIR="${3}" # Absolute path to folder containing VCFs
VERSION="${4}" # Version of database. Determined in 'make_database.sh'
SIMG="${5}" # Absolute path to Singularity container that contains executable 'create-database'

# Make database
if [ -d "$VCF_DIR" ] && [ -n "$INDB_NAME" ] && [ -d "$INDB_DIR" ]; then
  >&2 echo "INFO:make_database:Bind $VCF_DIR to /vcf and build database"
  singularity exec -H $INDB_DIR -B ${VCF_DIR}:/vcf $SIMG create-database \
    /vcf \
    ${INDB_DIR}/${INDB_NAME}_$VERSION
else
  >&2 echo "ERROR:make_database:Check that $VCF_DIR is a directory"
  >&2 echo "ERROR:make_database:Check that $INDB_NAME is not empty"
  >&2 echo "ERROR:make_database:Check that $INDB_DIR is a directory"
  exit 1
fi
