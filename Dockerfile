FROM ubuntu:focal-20210416

RUN apt-get update && \
    apt-get install -y \
    vcftools \
    git \
    git-lfs \
    python3-pip \
    bedtools \
    rsync \
    libcurl4

WORKDIR /work

COPY . .

RUN pip3 install --no-cache-dir -r requirements.txt

ENV LC_COLLATE=C
ENV LANGUAGE=C
ENV LC_ALL=C

ENV PATH="/work/bin:${PATH}"

ENTRYPOINT [ "/bin/bash" ]
